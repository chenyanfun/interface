# 公共类接口展示

### 日志接口展示
#### 请求方式
`POST`
#### 请求路由
`common/getLoginfo`
#### 请求参数
- token:用户登录认证返回token；
- page:当前请求页数；
- pagesize:当前请求大小

#### 返回结果
```json
{
    "message": "success",
    "data": [
        {
            "id": 1,
            "name": "用户登录日志",
            "time": "2019-08-08 03:48:23",
            "username": "admin",
            "content": "用户成功登录系统"
        },
        {
            "id": 2,
            "name": "用户注销日志",
            "time": "2019-08-08 06:36:32",
            "username": "admin",
            "content": "用户成功推出系统"
        }
    ],
    "totalnum": 34
}
```

### 点位日志接口展示
#### 请求方式
`POST`
#### 请求路由
`common/getPointLog`
#### 请求参数


#### 返回结果
```json
{
	"message": "success",
	"data": [{
		"username": "admin",
		"log": "上传梦幻岛工程学校污水监测2数据9条",
		"time": "2019-08-13 11:40:28"
	}, {
		"username": "admin",
		"log": "上传兴隆湖地下水监测数据2条",
		"time": "2019-08-13 11:29:28"
	}, {
		"username": "admin",
		"log": "上传兴隆湖地下水监测数据4条",
		"time": "2019-08-13 11:13:41"
	}]
}
```
