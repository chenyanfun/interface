# 天晟院环保大数据平台

## 用户管理

#### 一、用户登录接口
##### 请求方式：
POST
##### 请求路由：
`accounts/login/`
##### 请求参数：

```json
{
"username":"admin",
"password":"abc123"
}
```
请求参数说明：
- username：用户名；
- password：密码

##### 返回结果：

```json
{
    "message": "success",
    "login_data": {
        "username": "admin",
        "token": "58b-276134c4d1b1915b9c51",
        "permission": [
            "增",
            "删",
            "改"
        ],
        "super": true
    }
}
```
 返回参数说明：
- message：表示请求信息是否成功，success表示请求成功；
- login_data:表示登录信息返回，username表示返回用户名，token表示请求认证秘钥,permission表示该用户可以使用的权限，super表示该用户是否为超级管理员；

#### 二、用户注销接口
##### 请求方式：
POST
##### 请求路由：
`accounts/logout/`
##### 请求参数：

```json
{
"token":"58b-8cf69ffce58e9ca90f34"
}
```
请求参数说明：
- token：用户登录时，后端传输的token值；

##### 返回结果：

```json
{
    "message": "success"
}
```
 返回参数说明：
- message：表示请求信息是否成功，success表示请求成功；
- login_data:表示登录信息返回，username表示返回用户名，token表示请求认证秘钥

##### 接口请求失败返回接口
```json
{
"message":"退出操作失败请检查后台代码"
}
```

#### 三、获取用户权限
##### 请求方式：
POST
##### 请求路由
`userprofile/queryUserPermission/`
##### 请求参数
```json
{
"token":"58p-ad3772a1a21bd2e0d87c"
}
```
- token:用户登录认证返回token
##### 返回结果
```json
{
    "message": "success",
    "data": {
        "username": "admin",
        "permission": [
            "增",
            "删",
            "改"
        ],
        "rolename": "超级管理员"
    }
}
```